# Hacklib - CS:GO #

**Built with [hacklib](https://bitbucket.org/rafzi/hacklib).**

This is a very basic visual overlay for CS:GO. It shows circles as the enemies heads. Primarily meant as minimal impressive example to show off hacklib.

## Building ##

Clone this repository into the src/ subdirectory of [hacklib](https://bitbucket.org/rafzi/hacklib) and rerun CMake.