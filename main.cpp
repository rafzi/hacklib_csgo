#include "hacklib/Main.h"
#include "hacklib/MessageBox.h"
#include "hacklib/WindowOverlay.h"
#include "hacklib/ImplementMember.h"
#include "hacklib/PatternScanner.h"
#include "hacklib/CrashHandler.h"

#ifdef _WIN32
#include "hacklib/DrawerD3D.h"
#else
#include "hacklib/DrawerOpenGL.h"
#endif


namespace CSGO
{
    enum Bone
    {
        Head = 6
    };
    class Entity
    {
#ifdef _WIN32
        IMPLMEMBER(int, TeamId, 0xf0);
        IMPLMEMBER(int, Health, 0xfc);
        IMPLMEMBER(hl::Vec3, Pos, 0x134);
        IMPLMEMBER(float*, Bones, 0x2698);
#else
        IMPLMEMBER(int, TeamId, 0x120);
        IMPLMEMBER(int, Health, 0x12c);
        IMPLMEMBER(hl::Vec3, Pos, 0x164);
        IMPLMEMBER(float*, Bones, 0x2c70);
#endif

        hl::Vec3 getBone(Bone i)
        {
            if (getBones())
            {
                float *bones = getBones();
                return hl::Vec3(bones[12*i + 3], bones[12*i + 7], bones[12*i + 11]);
            }
            else
            {
                return hl::Vec3(0.0f);
            }
        }
    };
}

class CSGOMain : public hl::Main
{
public:
    bool init() override;
    bool step() override;

private:
    hl::WindowOverlay m_overlay;
#ifdef _WIN32
    hl::DrawerD3D m_drawer;
#else
    hl::DrawerOpenGL m_drawer;
#endif

    struct Mems {
        CSGO::Entity **ownEntity;
        CSGO::Entity ***entityList;
        hl::Mat4x4 *viewProjMat;
    } m_mems;
};

hl::StaticInit<CSGOMain> g_initObj;


bool CSGOMain::init()
{
#ifdef _WIN32
    uintptr_t ownEntity = hl::FindPattern("42 56 8d 34 85", "client.dll");
    uintptr_t entityList = hl::FindPattern("8d 49 00 83 fe 01", "client.dll");
    uintptr_t viewMat = hl::FindPattern("50 f3 0f 7f 85 40 ff ff ff", "client.dll");
#else
    uintptr_t ownEntity = hl::FindPattern("55 83 ff ff 48 8b 05", "csgo/bin/linux64/client_client.so");
    uintptr_t entityList = hl::FindPattern("48 03 47 ?? 8b 00 89 45 ?? 48 8b 05", "csgo/bin/linux64/client_client.so");
    uintptr_t viewMat = hl::FindPattern("48 8b 3d ?? ?? ?? ?? 31 d2 4c 89 e6 e8", "csgo/bin/linux64/client_client.so");
#endif

    if (!ownEntity || !entityList || !viewMat) {
        hl::MsgBox("Error", "Patterns not found");
        return false;
    }

    hl::CrashHandler([=]{
#ifdef _WIN32
        m_mems.ownEntity = (CSGO::Entity**)(*(uintptr_t*)(ownEntity + 0x5) + 0x4);
        m_mems.entityList = (CSGO::Entity***)(entityList - 0x4);
        m_mems.viewProjMat = (hl::Mat4x4*)(*(uintptr_t*)(viewMat + 0xd) + 0x90);
#else
        m_mems.ownEntity = (CSGO::Entity**)(hl::FollowRelativeAddress(ownEntity + 0x7));
        m_mems.entityList = (CSGO::Entity***)(*(uintptr_t*)hl::FollowRelativeAddress(entityList + 0xc));
        m_mems.viewProjMat = (hl::Mat4x4*)(*(uintptr_t*)hl::FollowRelativeAddress(viewMat + 0x3) + 0x2a4);
#endif
    }, [](uint32_t code){
        hl::MsgBox("Error", "Invalid patterns");
    });

    if (m_overlay.create() != hl::WindowOverlay::Error::Okay) {
        hl::MsgBox("Error", "Could not create overlay");
        return false;
    }

    m_overlay.setTargetRefreshRate(100);
    m_overlay.registerResetHandlers([&]{ m_drawer.onLostDevice(); }, [&]{ m_drawer.onResetDevice(); });
    m_drawer.setContext(m_overlay.getContext());

    return true;
}

bool CSGOMain::step()
{
#ifdef _WIN32
    if (GetAsyncKeyState(VK_HOME) < 0)
        return false;
#endif

    auto ownEntity = *m_mems.ownEntity;
    auto entityList = *m_mems.entityList;
    if (!ownEntity || !entityList)
        return true;

#ifndef _WIN32
    entityList++;
#endif

    hl::Mat4x4 viewProjMat = glm::transpose(*m_mems.viewProjMat);
    hl::Mat4x4 identity(1.0f);
    m_drawer.update(viewProjMat, identity);

    auto myPos = ownEntity->getPos();

    m_overlay.beginDraw();

    for (auto ppEntity = entityList; *ppEntity; ppEntity += 4)
    {
        if ((*ppEntity)->getHealth() == 0 || (*ppEntity)->getTeamId() == ownEntity->getTeamId())
            continue;

        auto worldPos = (*ppEntity)->getBone(CSGO::Bone::Head);
        hl::Vec3 screenPos;
        m_drawer.project(worldPos, screenPos);
        if  (m_drawer.isInfrontCam(screenPos))
        {
            float dist = glm::length(myPos - worldPos);
            m_drawer.drawCircle(screenPos.x, screenPos.y, 5000*(1/dist), hl::Color(0, 255, 0));
        }
    }

    m_overlay.swapBuffers();

    return true;
}
